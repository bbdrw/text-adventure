function checkDescription(adId, eventName) {

    if ($("#eventDescription").val() == "") {
        alert("please fill out a description for the event before adding a key!");
        focusElement($("#eventDescription"));
    }
    else{
        keyStartCheck(adId, eventName);
    }
}

function deleteKey(keyId){
    var options = [keyId, localStorage.getItem("eventId")];
    function callback() {
        displayKeyTable(localStorage.getItem("eventId"));
    }
    textAdKey.deleteKey(options, callback);
}

function selectCopy(key){
    var options = [key];
    var emptyKey = false;
    var keyArray = [];

    function callback(tx, results) {
        var row = results.rows[0];

        if(row.length == 0 || row.length == ""){
            emptyKey = true;
        }
        else{
            keyArray[0] = row['name'];
            keyArray[1] = row['action'];
            keyArray[2] = row['connectedEvent'];
            keyArray[3] = row['connectedEventName'];
            keyArray[4] = row['verb'];
            keyArray[5] = row['noun'];
        }


        if(emptyKey == false){
            var keyCopyArray = [];
            var keyStr = "";
            var options2 = [keyArray[0], localStorage.getItem("eventId")];

            function callback2(tx, results2) {
                for (var i = 0; i < results2.rows.length; i++) {
                    var row2 = results2.rows[i];
                    keyCopyArray[i] = row2['name'];

                    keyStr += row2['name'];
                }

                keyArray[0] = keyCopyArray[keyCopyArray.length - 1];
                insertKey(keyArray);

            }
            textAdKey.selectCopy(options2, callback2);
        }

    }
    textAdKey.select(options, callback);
}

function insertKey(keyArray) {

    keyArray[0] = keyArray[0] + "COPY";

    var options = [localStorage.getItem("eventId"), keyArray[0], keyArray[1], keyArray[2], keyArray[3], keyArray[4], keyArray[5]];

    function callback(tx, results) {
        displayKeyTable(localStorage.getItem("eventId"));
    }
    textAdKey.insert(options, callback);
}

function firstStepKey(copiedId, keyArray){
    saveKey(copiedId, keyArray)
}

function saveKey(copiedId, keyArray){
    var position = 0;
    if(localStorage.getItem("copyKeySaves") == null || localStorage.getItem("copyKeySaves") == ""){
        position = 0;
        localStorage.setItem("copyKeySaves", "0");
    }
    else{
       position = parseInt(localStorage.getItem("copyKeySaves")) + 1;
       localStorage.setItem("copyKeySaves", position.toString());
    }

    var keyLength = 0;

    keyLength = keyArray.length;

    if(position < keyLength){
        loopArray(keyArray[position][0],keyArray[position][1],keyArray[position][2],keyArray[position][3],keyArray[position][4],keyArray[position][5],copiedId, keyArray );
    }
    else{
        localStorage.setItem("copyKeySaves", "");
        loadEventTable(localStorage.getItem("adId"));
    }
}

function loopArray(name, action, connectedEvent, connectedEventName,verbs, nouns, copiedId, keyArray){
    var options = [copiedId,name,action,connectedEvent,connectedEventName, verbs, nouns];
    function callback() {
        firstStepKey(copiedId, keyArray);
    }
    textAdKey.insert(options, callback);
}

function displayKeyModal(keyId){
    $("form")[1].reset();
    $("#keyEvent").hide();
    $("#keyAction").show();

    $("#keyEventDesc").html("<b>" + localStorage.getItem("eventName") + "<b>");
    //checkDescription($("#keyModal"));

    //check to see if there is a key with the id passed in
    var options = [keyId];

    function callback(tx, results) {

        console.info("Success: Record inserted successfully");
        var row = results.rows[0];

        if(row != null && row != ""){
            $("#updateKeyBtn").show();
            $("#newKeyBtn").hide();
            $("#keyHeader").html("Edit Key");

            $("#verb").val(row['verb']);
            $("#noun").val(row['noun']);

            if(row['action'] == "EMPTY"){
                //this is if the user decided to add an event not an action
                $("#key").val(row['name']);
                $('#keyCheckBox').prop('checked', true).checkboxradio('refresh');
                $("#keyEvent").show();
                $("#keyAction").hide();

                fillDropDownUpdate(row['connectedEvent']);

            }
            else{
                //this is if the user decided to add an action to the key not an event!
                $("#key").val(row['name']);
                $('#keyCheckBox').prop('checked', false).checkboxradio('refresh');
                $("#keyEvent").hide();
                $("#keyAction").show();

                $("#action").val(row['action']);
                $("#keyModal").show();
            }

        }
        else{

            $("#updateKeyBtn").hide();
            $("#newKeyBtn").show();

            $("form")[1].reset();
            $("#keyHeader").html("Add Key");
            $("#keyEvent").hide();
            $("#keyAction").show();
            $("#keyModal").show();
        }
    }
    textAdKey.select(options, callback);
}

function displayKeyTable(eventNum, location){
    var options2 = [eventNum];

    function callback2(tx, results) {
        console.info("Success: Record inserted successfully");

        var htmlcode = "<tr>" +
            "<th>Key</th>" +
            "<th>Verb(s)</th>" +
            "<th>Noun(s)</th>" +
            "<th>Event/Action</th>" +
            "</tr>";
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];
            var action = "";
            if (row['action'] == "EMPTY") {
                action = row['connectedEventName'];
            } else {
                action = row['action'];
            }

            htmlcode += "<tr  data-row-id=" + row['id'] + ">" +
                "<td width='35%'>" + row['name'] + "</td>" +
                "<td width='20%'>" + row['verb'] + "</td>"+
                "<td width='20%'>" + row['noun'] + "</td>"+
                "<td width='15%'>" + action + "</td>"+
                "<td width='12%' style='text-align: center;'><button type='button' data-inline='true' class='keys' data-row-id=" + row['id'] + " data-role='button'>Edit</button>"+
                "<button type='button' data-inline='true' class='delete' data-row-id=" + row['id'] + " data-role='button'>Delete</button>" +
                "<button type='button' data-inline='true' class='copy' data-row-id=" + row['id'] + " data-role='button'>Copy</button></td>" +
                "</tr>";
        }

        var eventTable = $("#keyTbl");
        eventTable.html(htmlcode);

        var element = document.getElementById("keyTableDiv");
        element.scrollTop = element.scrollHeight;

        $(".copy").on("click", clickHandler3);

        function clickHandler3() {
            var key = $(this).attr("data-row-id");
            //lookupKey(key);
            selectCopy(key);
        }

        $(".delete").on("click", clickHandler2);

        function clickHandler2() {
            var key = $(this).attr("data-row-id");
            deleteKey(key);
        }

        $(".keys").on("click", clickHandler);

        function clickHandler() {
            localStorage.setItem("keyid", $(this).attr("data-row-id") );
            displayKeyModal(localStorage.getItem("keyid"));


        }

        if (location == 1){
            $("#keyModal").hide();
        }
    }
    textAdEvent.selectAllKey(options2, callback2);
}
