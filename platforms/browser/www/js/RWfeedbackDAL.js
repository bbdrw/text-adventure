/**
 * File Name: RWfeedbackDAL.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-02 : Created
 */


var Review ={
    RWinsert: function (options, callback) {
        function txFunction(tx) {
            var sql = "INSERT INTO review(businessName, typeId, reviewerEmail, reviewerComments, reviewDate, hasRating, rating1, rating2, rating3) VALUES(?,?,?,?,?,?,?,?,?);";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    RWupdate: function (options, callback) {
        function txFunction(tx) {
            var sql = "UPDATE review SET businessName = ?, typeId = ?, reviewerEmail = ?, reviewerComments = ?, reviewDate = ?, hasRating = ?, rating1 = ?, rating2 =?," +
                "rating3 = ?  WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: update transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    RWdelete: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM review WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    RWselect: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM review WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    RWselectAll: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM review;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};

var Type = {

    RWselectAll: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM type;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }

};