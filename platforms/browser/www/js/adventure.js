function addAdventure(userId, adName, adDesc){
    var options = [userId, adDesc, adName];

    function callback() {
        console.info("Success: Record inserted successfully");

        $("#adventurename").val("");
        $("#adDesc").val("");

        loadAdventureTable(userId);
    }
    textAdventure.insert(options, callback);
}

function findEvents(adventure){
    localStorage.setItem("adventureClick", adventure);
    var options = [adventure];
    function callback(tx, results) {
        var eventArray = [];
        var rowCheck = results.rows.length;

        if(rowCheck > 0) {
            for (var i = 0; i < results.rows.length; i++) {
                var row = results.rows[i];
                eventArray[i] = row['id'];
            }

            goThroughEvents(eventArray)
        }
        else{
            adventureDeleteEvents();
        }

    }
    textAdEvent.selectAdventureDelete(options, callback)
}

function goThroughEvents(eventArray){
    var eventAt = 0;
    if(localStorage.getItem("eventAt") == null || localStorage.getItem("eventAt") == ""){
        eventAt = 0;
        localStorage.setItem('eventAt', eventAt.toString());
    }
    else{
        eventAt = parseInt(localStorage.getItem("eventAt")) + 1;
        localStorage.setItem('eventAt', eventAt.toString());
    }

    if(eventAt < eventArray){
        deleteKeys(eventArray[eventAt]);
    }
    else{
        adventureDeleteEvents();
    }

}

function deleteKeys(eventId){

    var options = [eventId];
    function callback(tx, results) {
        var rowTest = results.rows.length;

        if(rowTest > 0){

            var sqlStatment = "DELETE FROM Key WHERE eventId = " + eventId + ";";

            var options2 = [sqlStatment];
            function callback2(tx, results) {
                adventureDeleteEvents();
            }
            textAdKey.adventureKeyDelete(options2, callback2);

        }



    }
    textAdKey.selectKeyDelete(options, callback);

}

function adventureDeleteEvents(){
    var options = [localStorage.getItem("adventureClick")];
    function callback(tx, results) {
        deleteAdventure();
    }
    textAdEvent.deleteEventAdventure(options, callback)
}


function deleteAdventure(){
    var options = [localStorage.getItem("adventureClick"), 1];
    function callback(tx, results) {
        loadAdventureTable(1);
    }
    textAdventure.delete(options, callback);
}


function loadAdventureTable(userId) {

    userId = 1;
    var options = [userId];

    function callback(tx, results) {
        console.info("Success: Record inserted successfully");
        var row2 = results.rows;

        var htmlcode = "<tr>" +
            "<th>Adventure Name</th>" +
            "<th>Description</th>" +
            "<th></th>" +
            "</tr>";

        if (row2.length === 0) {

            var adventureTbl = $("#AdventureTbl");
            adventureTbl.html(htmlcode);

            if(localStorage.getItem("adStart") == null || localStorage.getItem("adStart") == ""){
                localStorage.setItem("adStart", "finished");
                adventureAssetsLoad();
            }

        }
        else {
            for (var i = 0; i < results.rows.length; i++) {
                var row = results.rows[i];

                var desc = "";
                if(row['description'] != null && row['description'] != "") {
                    desc = row['description'];
                }

                htmlcode += "<tr>" +
                    "<td width='45%'>" + row['name'] + "</td>" +
                    "<td width='35%'>" + desc + "</td>" +
                    "<td width='15%' style='text-align: center;'><button type='button' data-inline='true' class='adventure' data-row-id=" + row['id'] + " data-role='button'>Edit</button>" +
                    "<button type='button' data-inline='true' class='delete' data-row-id=" + row['id'] + " data-role='button'>Delete</button>" +
                    "<button type='button' data-inline='true' class='copy' data-row-id=" + row['id'] + " data-role='button'>Copy</button></td>" +
                    "</tr>";

            }

            var adventureTbl = $("#AdventureTbl");
            adventureTbl.html(htmlcode);

            var element = document.getElementById("adventureTableDiv");
            element.scrollTop = element.scrollHeight;

            $(".delete").on("click", clickHandler2);

            function clickHandler2() {
                var adventure = $(this).attr("data-row-id");
                findEvents(adventure);
            }

            $(".copy").on("click", clickHandler3);

            function clickHandler3() {
                var adventure = $(this).attr("data-row-id");
                alert("work in progress");
            }

            $(".adventure").on("click", clickHandler);

            function clickHandler() {
                localStorage.setItem("adId", $(this).attr("data-row-id"));
                var options = [$(this).attr("data-row-id"), 1];

                function callback(tx, results) {
                    console.info("Success: Record inserted successfully");


                    var row = results.rows[0];

                    $("#adventureHeaderName").html(row['name']);
                    $("#adventureHeaderDescription").html(row['description']);


                    $.mobile.changePage("#EventPage", {transition: 'fade'});
                    start();

                }
                textAdventure.select(options, callback);


            }
            if(localStorage.getItem("adStart") == null || localStorage.getItem("adStart") == ""){
                localStorage.setItem("adStart", "finished");
                adventureAssetsLoad();
            }
        }

    }
    textAdventure.selectAll(options, callback);
}