/**
 * File Name: database.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-02 : Created
 */


var db;

/**
 * General purpose error handler
 * @param tx The transaction object
 * @param error The error object
 */
function errorHandler(tx, error) {
    console.error("SQL Error: " + tx + " (" + error.code + ") : " + error.message);
}



var DB = {
    RWcreateDatabase: function(){
        var name= "RW Business Rater";
        var version = "1.0";
        var displayName = "DB for RW Busines Rater app";
        var size =  2 * 1024 * 1024;

        function successCreate() {
            console.info("Success: Database created successfully");
        }

        db = openDatabase(name, version, displayName, size, successCreate);
    },
    RWcreateTables: function(){
        function txFunction(tx) {

            var sql = "CREATE TABLE IF NOT EXISTS type( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "name VARCHAR(20) NOT NULL);";

            var sql3 = "INSERT INTO type (name) VALUES('Canadian'), ('Asian'), ('Other')"

            var sql2 = "CREATE TABLE IF NOT EXISTS review( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "businessName VARCHAR(30) NOT NULL, " +
                "typeId INTEGER NOT NULL, " +
                "reviewerEmail VARCHAR(30), " +
                "reviewerComments TEXT, " +
                "reviewDate DATE, " +
                "hasRating VARCHAR(1), " +
                "rating1 INTEGER, " +
                "rating2 INTEGER, " +
                "rating3 INTEGER, " +
                "FOREIGN KEY(typeId) REFERENCES type(id));";

            var myArray = [sql, sql3, sql2];

            for(i = 0; i < myArray.length; i++){

                var options = [];

                function successCreate() {
                    console.info("Success: Table created successfully.");
                }

                tx.executeSql(myArray[i], options, successCreate, errorHandler);

            }

        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
    RWdropTables: function () {

        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS type;";
            var sql2 = "DROP TABLE IF EXISTS review;";

            var myArray = [sql, sql2];
            for(i = 0; i < myArray.length; i++) {

                var options = [];

                function successDrop() {
                    console.info("Success: table dropped successfully");
                    //alert("database cleared successfully");
                }

                tx.executeSql(myArray[i], options, successDrop, errorHandler);
            }
        }
        function successTransaction() {
            console.info("Success: drop table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};





















