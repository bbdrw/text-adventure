/**
 * File Name: RWfeedbackDAL.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-02 : Created
 */

var textAdventure = {
  insert: function (options, callback) {
      function txFunction(tx) {
          var sql = "INSERT INTO Adventure (userId,description,name) VALUES (?,?,?)";

          tx.executeSql(sql, options, callback, errorHandler);
      }
      function successTransaction() {
          console.info("Success: Insert transaction successful");
      }
      db.transaction(txFunction, errorHandler, successTransaction);
  },

    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Adventure WHERE id=? AND userId = ?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectCheck: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Adventure WHERE name =? AND userId = ?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectCopy: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Adventure WHERE name LIKE '%' || ? || '%' AND userId = ? ORDER BY name;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
  selectAll: function (options, callback) {
      function txFunction(tx) {
          var sql = "SELECT * FROM Adventure WHERE userId = ?;";

          tx.executeSql(sql, options, callback, errorHandler);
      }
      function successTransaction() {
          console.info("Success: selectAll transaction successful");
      }
      db.transaction(txFunction, errorHandler, successTransaction);
  },
    delete: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM Adventure WHERE id=? AND userId = ?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }


};

var textAdEvent = {

  insert: function (options, callback) {
      function txFunction(tx) {
          var sql = "INSERT INTO Event (adId,description) VALUES (?,?)";

          tx.executeSql(sql, options, callback, errorHandler);
      }
      function successTransaction() {
          console.info("Success: Insert transaction successful");
      }
      db.transaction(txFunction, errorHandler, successTransaction);
  },
    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Event WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectCheck: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Event WHERE adId =? AND description = ?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectEvent: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Event WHERE adId=? AND description = ?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectCopy: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Event WHERE adId=? AND id = ?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAdventureDelete: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Event WHERE adId=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectEventCopy: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Event WHERE description LIKE '%' || ? || '%' AND adId = ? ORDER BY description;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
  selectAll: function (options, callback) {
      function txFunction(tx) {
          var sql = "SELECT * FROM Event;";

          tx.executeSql(sql, options, callback, errorHandler);
      }
      function successTransaction() {
          console.info("Success: selectAll transaction successful");
      }
      db.transaction(txFunction, errorHandler, successTransaction);
  },
  selectAllKey: function (options, callback) {
      function txFunction(tx) {
          var sql = "SELECT * FROM Key WHERE eventId = ?;";

          tx.executeSql(sql, options, callback, errorHandler);
      }
      function successTransaction() {
          console.info("Success: selectAll transaction successful");
      }
      db.transaction(txFunction, errorHandler, successTransaction);
  },
  selectAllEvents: function (options, callback) {
      function txFunction(tx) {
          var sql = "SELECT * FROM Event WHERE adId = ?;";

          tx.executeSql(sql, options, callback, errorHandler);
      }
      function successTransaction() {
          console.info("Success: selectAll transaction successful");
      }
      db.transaction(txFunction, errorHandler, successTransaction);
  },
  update: function (options, callback) {
      function txFunction(tx) {
          var sql = "UPDATE Event SET description = ? WHERE id=?;";

          tx.executeSql(sql, options, callback, errorHandler);
      }
      function successTransaction() {
          console.info("Success: update transaction successful");
      }
      db.transaction(txFunction, errorHandler, successTransaction);
  },
    deleteEventAdventure: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM Event WHERE adId = ?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
  delete: function (options, callback) {
      function txFunction(tx) {
          var sql = "DELETE FROM Event WHERE id=? AND adId = ?;";

          tx.executeSql(sql, options, callback, errorHandler);
      }
      function successTransaction() {
          console.info("Success: delete transaction successful");
      }
      db.transaction(txFunction, errorHandler, successTransaction);
  }

};

var textAdKey = {

  insert: function (options, callback) {
      function txFunction(tx) {
          var sql = "INSERT INTO Key(eventId, name, action, connectedEvent, connectedEventName, verb, noun) VALUES(?,?,?,?,?,?,?);";

          tx.executeSql(sql, options, callback, errorHandler);
      }
      function successTransaction() {
          console.info("Success: Insert transaction successful");
      }
      db.transaction(txFunction, errorHandler, successTransaction);
  },
    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Key WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectCheck: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Key WHERE eventId =? AND name = ?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectKeyDelete: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Key WHERE eventId=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectCopy: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Key WHERE name LIKE '%' || ? || '%' AND eventId = ? ORDER BY name;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAll: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM Key WHERE eventId=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    update: function (options, callback) {
        function txFunction(tx) {
            var sql = "UPDATE Key SET name = ?, action = ?, connectedEvent = ?, connectedEventName = ?, verb = ?, noun = ? WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: update transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    delete: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM Key WHERE eventId=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    deleteKey: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM Key WHERE id=? AND eventId=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    adventureKeyDelete: function (options, callback) {
        function txFunction(tx) {
            var sql = options[0];

            tx.executeSql(sql, [], callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },

};

var textAdConnectedEvent = {

};



/*var Review ={
    RWinsert: function (options, callback) {
        function txFunction(tx) {
            var sql = "INSERT INTO review(businessName, typeId, reviewerEmail, reviewerComments, reviewDate, hasRating, rating1, rating2, rating3) VALUES(?,?,?,?,?,?,?,?,?);";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    RWupdate: function (options, callback) {
        function txFunction(tx) {
            var sql = "UPDATE review SET businessName = ?, typeId = ?, reviewerEmail = ?, reviewerComments = ?, reviewDate = ?, hasRating = ?, rating1 = ?, rating2 =?," +
                "rating3 = ?  WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: update transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    RWdelete: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM review WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    RWselect: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM review WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    RWselectAll: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM review;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};

var Type = {

    RWselectAll: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM type;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }

};*/
