function validateEventName(adId, description, eventModal){

    var options = [adId, description];

    function callback(tx, results){
        var row = results.rows;

        if(row.length >= 1){
            alert("the event description put in is already taken!");
            focusElement($("#eventname"));

        }
        else{
            var options = [parseInt(localStorage.getItem("adId")),$("#eventname").val()];

            function callback() {
                console.info("Success: Record inserted successfully");

                var options2 = [parseInt(localStorage.getItem("adId")),$("#eventname").val()];
                function callback2(tx, results2) {
                    console.info("Success: Record inserted successfully");
                    var row = results2.rows[0];

                    localStorage.setItem('eventId', row['id']);
                    localStorage.setItem('eventName', $("#eventname").val());

                    $("form")[0].reset();
                    $("#keyTbl").empty();
                    $("#eventHeader").html("Add Event");
                    $("#eventDescription").val(localStorage.getItem("eventName"));

                    $("#eventname").val("");

                    eventModal.show();

                    $("#keyTbl").html("");
                    displayKeyTable(localStorage.getItem("eventId"), 0);
                }
                textAdEvent.selectEvent(options2, callback2);

            }
            textAdEvent.insert(options, callback);
        }

    }
    textAdEvent.selectCheck(options, callback);

}

function validateNameOnSaveEvent(adId, description, eventModal){
    var options = [adId, description];

    function callback(tx, results){
        var row = results.rows;

        if(row.length >= 1){

            var rowLength = row.length.toString();

            if(rowLength == "1") {

                var rowCheck = results.rows[0];

                if (rowCheck['id'] == localStorage.getItem("eventId")) {

                    var options = [$("#eventDescription").val(), parseInt(localStorage.getItem("eventId"))];
                    localStorage.setItem("eventName", $("#eventDescription").val());

                    function callback() {
                        console.info("Success: Record inserted successfully");
                        //displayKeyModal();
                        eventModal.hide();
                        localStorage.setItem("eventOpen", "full");
                        loadEventTable(parseInt(localStorage.getItem("adId")));

                    }
                    textAdEvent.update(options, callback);

                }
                else{
                    alert("the event description put in is already taken!");
                    focusElement($("#eventDescription"));
                }
            }
            else{
                alert("the event description put in is already taken!");
                focusElement($("#eventDescription"));
            }

        }

    }
    textAdEvent.selectCheck(options, callback);
}