function validateKeyName(name,verbs, nouns, eventId){
    if(name == null || name == ""){
        alert("the key name put in cannot be blank!");
        focusElement($("#key"));
    }
    else{

        var isEmpty = false;
        var warningMessage = "";

        if(verbs == null || verbs == ""){
            warningMessage += "There needs to be at least 1 verb! ";
            isEmpty = true;

            focusElement($("#verb"));
        }
        if(nouns == null || nouns == ""){
            warningMessage += "There needs to be at least 1 noun! ";

            if(isEmpty == false){
                isEmpty = true;
                focusElement($("#noun"));
            }
        }
        else{

            var dict = {};
            var verbList = [];

            var foundCopy = false;

            verbs = verbs.toLowerCase();
            verbs = verbs.replace(" ", "");

            if(verbs.indexOf(",") > -1){
                verbList = verbs.split(",");

                for(var i = 0; i < verbList.length; i++){

                    if(verbList[i] in dict){
                        foundCopy = true;
                        isEmpty = true;
                        warningMessage = "There cannot be multiples of a word in your verb list! ";
                        focusElement($("#verb"));
                        break;

                    }
                    else{
                        dict[verbList[i]] = "HIT";
                    }

                }
            }


            dict = {};
            var nounList = [];

            nouns = nouns.toLowerCase();
            nouns = nouns.replace(" ", "");

            if(nouns.indexOf(",") > -1){
                nounList = nouns.split(",");

                for(var i = 0; i < nounList.length; i++){

                    if(nounList[i] in dict){
                        foundCopy = true;
                        warningMessage = "There cannot be multiples of a word in your noun list! ";
                        if(foundCopy == false){
                            focusElement($("#noun"));
                            isEmpty = true;
                            foundCopy = true;
                        }

                        break;

                    }
                    else{
                        dict[nounList[i]] = "HIT";
                    }

                }
            }

        }

        if(isEmpty == true){
            alert(warningMessage);
        }
        else{
            var options = [eventId, name];

            function callback(tx, results){
                var row = results.rows;

                var rowLength = row.length.toString();

                if(row.length >= 1){

                    if(rowLength == "1"){

                        var rowCheck = results.rows[0];


                        if(rowCheck['id'] == localStorage.getItem("keyid")){

                            if ($("#keyCheckBox").is(":checked")) {
                                validateKeyAction(name,eventId, verbs, nouns);
                            }
                            else{
                                validateKeyEvent(name,eventId, verbs, nouns);
                            }
                        }

                        else{
                            alert("the key put in is already taken!");
                            focusElement($("#key"));
                        }

                    }
                    else{
                        alert("the key put in is already taken!");
                        focusElement($("#key"));
                    }

                }
                else{
                    if ($("#keyCheckBox").is(":checked")) {
                        validateKeyAction(name,eventId, verbs, nouns);
                    }
                    else{
                        validateKeyEvent(name,eventId, verbs, nouns);
                    }
                }

            }
            textAdKey.selectCheck(options, callback);
        }


    }


}

function validateKeyAction(name, eventId, verbs, nouns){
    if($('#eventDrop option:selected').text() == localStorage.getItem("eventName")){
        alert("the event chosen can not be the same one you are editing!");
        focusElement($("#eventDrop"));
    }
    else if($('#eventDrop option:selected').text() == "...Please select one of the events"){
        alert("you need to choose a valid event!");
        focusElement($("#eventDrop"));
    }
    else{

        var options = [eventId, name, "EMPTY", $('#eventDrop option:selected').val(), $('#eventDrop option:selected').text(), verbs, nouns];

        if(localStorage.getItem("keyLocation") == "1"){
            options = [name, "EMPTY", $('#eventDrop option:selected').val(), $('#eventDrop option:selected').text(), verbs, nouns, localStorage.getItem("keyid")];
            keySuccessUpdate(options, eventId)
        }
        else{
            keySuccess(options, eventId)
        }
    }
}

function validateKeyEvent(name, eventId, verbs, nouns){
    if($("#action").val() == null || $("#action").val() == ""){
        alert("there needs to be an action!");
        focusElement($("#action"));
    }
    else{
        var options = [eventId, name, $("#action").val(), "EMPTY", "EMPTY", verbs, nouns];

        if(localStorage.getItem("keyLocation") == "1"){
            options = [name, $("#action").val(), "EMPTY", "EMPTY", verbs, nouns,localStorage.getItem("keyid") ];
            keySuccessUpdate(options, eventId)
        }
        else{
            keySuccess(options, eventId)
        }

    }
}

function keySuccess(options, eventId){
    function callback() {
        console.info("Success: Record inserted successfully");
        var options2 = [eventId];

        function callback2(tx, results) {
            console.info("Success: Record inserted successfully");
            $("#keyTbl").html("");
            displayKeyTable(eventId, 1);

        }
        textAdEvent.selectAllKey(options2, callback2);

    }
    textAdKey.insert(options, callback);
}

function keySuccessUpdate(options){
    function callback() {
        console.info("Success: Record inserted successfully");
        displayKeyTable(localStorage.getItem("eventId"), 1);
    }

    textAdKey.update(options, callback);
}

function keyStartCheck(adId, description){

    var options = [adId, description];

    function callback(tx, results){
        var row = results.rows;

        if(row.length >= 1){

            var rowLength = row.length.toString();

            if(rowLength == "1") {

                var rowCheck = results.rows[0];

                if (rowCheck['id'] == localStorage.getItem("eventId")) {

                    var options = [$("#eventDescription").val(), parseInt(localStorage.getItem("eventId"))];
                    localStorage.setItem("eventName", $("#eventDescription").val());

                    function callback() {
                        console.info("Success: Record inserted successfully");

                        displayKeyModal();

                    }
                    textAdEvent.update(options, callback);

                }
                else{
                    alert("the event description put in is already taken!");
                    focusElement($("#eventDescription"));
                }
            }
            else{
                alert("the event description put in is already taken!");
                focusElement($("#eventDescription"));
            }

        }
        else{
            displayKeyModal();
        }

    }
    textAdEvent.selectCheck(options, callback);

}