//Created By: Ryan Wing

$(document).ready(function() {
  //when the page is loaded call the init method

  initDB();
  adventureStart();

});

function initDB() {
  console.info("Creating Database ...");
  try {
    DB.createDatabase();
    if (db) {
      DB.dropTables();
      DB.createTables();
    } else {
      console.error("Error: Cannot create tables : Database not available");
    }
  } catch (e) {
    console.error("Error: (Fatal) Error in initDB(). Cannot proceed.");
  }
}

function start(){
    var id = 1;
    localStorage.setItem("UserId", id.toString());
    loadEventTable(localStorage.getItem("adId"));
}

function adventureStart(){
    localStorage.clear();
    loadAdventureTable(1);
}

function focusElement(element){
    element.focus();
}

function adventureAssetsLoad(){
    $("#adventurebutton").on("click", function() {
        var adventureName = $("#adventurename").val();
        var adventureDesc = $("#adDesc").val();
        if(adventureName === null || adventureName === ""){
            alert("you must have an adventure name");
            focusElement($("#adventurename"));
        }
        else {
            validateAdventureName(adventureName, adventureDesc);
        }

    });
}

function init() {

    // $("#editAdventureName").hide();
    //
    // $("#adventureHeaderName").hover(function(){
    //     $("#editAdventureName").show();
    // }, function(){
    //     $("#editAdventureName").hide();
    // });
    //
    // $("#editAdventureName").hover(function(){
    //     $("#editAdventureName").show();
    // }, function(){
    //     $("#editAdventureName").hide();
    // });

    var userId = localStorage.getItem("UserId");

    var eventModal = $("#eventModal");

    if(localStorage.getItem("adventureButtonStart") == null || localStorage.getItem("adventureButtonStart") == "") {
        localStorage.setItem("adventureButtonStart", "STARTED");
        //*****Going Back to Adventure Page******//
        $("#backAdventureButton").on("click", function () {
            $.mobile.changePage("#AdventurePage", {transition: 'fade'});
            $("#adventurename").val("");
            //localStorage.setItem("firstStart", "");
            loadAdventureTable(1);

        });
    }


    if(localStorage.getItem("eventButtonStart") == null || localStorage.getItem("eventButtonStart") == "") {
        localStorage.setItem("eventButtonStart", "STARTED");
        //******this portion is for adding events******
        //var eventModal = $("#eventModal");

        $("#eventButton").on("click", function () {

            var eventName = $("#eventname").val();
            if (eventName === null || eventName === "") {
                alert("you must have an event name");
                focusElement($("#eventname"));
            }
            else {

                validateEventName(localStorage.getItem("adId"), localStorage.getItem("eventName"), eventModal);
            }

        });
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      //eventModal.style.display = "none";
      loadEventTable(localStorage.getItem("adId"));
      eventModal.hide();
    };

    //*****saveEvent*****
    $("#saveEvent").on("click", function() {

        if($("#eventDescription").val() == null || $("#eventDescription").val() == ""){
            alert("there must be an event description!");
            focusElement($("#eventDescription"));
        }
        else{

            validateNameOnSaveEvent(localStorage.getItem("adId") ,localStorage.getItem("eventName"), eventModal);
        }

    });

    //******this portion is for adding keys******
    var keyModal = $("#keyModal");
    $("#updateKeyBtn").hide();
    $("#newKeyBtn").show();
    $("#addKey").on("click", function() {
      checkDescription(localStorage.getItem("adId") ,$("#eventDescription").val());

    });
    // Get the <span> element that closes the modal
    var keyspan = document.getElementsByClassName("close")[1];

    // When the user clicks on <span> (x), close the modal
    keyspan.onclick = function() {
      keyModal.hide();
    };

    $("#updateKeyBtn").on("click", function(){
        //this is where the update of the key will be
        localStorage.setItem("keyLocation", "1");

        var name = $("#key").val();

        //*****ADDITIONAL ADDING VALUES*****

        var verbs = $("#verb").val();
        var nouns = $("#noun").val();


        var sameEvent = false;
        var keyid = localStorage.getItem("keyid");
        var eventId = localStorage.getItem("eventId");
        var options = [];

        validateKeyName(name, verbs, nouns,  eventId);

    });

    $("#newKeyBtn").on("click", function() {
      //eventId, name, action, connectedEvent
      var name = $("#key").val();

      //*****ADDITIONAL ADDING VALUES*****

     var verbs = $("#verb").val();
     var nouns = $("#noun").val();

      var sameEvent = false;
      var eventId = localStorage.getItem("eventId");

        localStorage.setItem("keyLocation", "2");

      validateKeyName(name,verbs, nouns, eventId);


    });

    //*****Check to see if the user wants to add another event to key*****
    $("#keyEvent").hide();
    $("#keyAction").show();

    $("#keyCheckBox").change(function() {
        fillDropDown();
    });


    //****This portion is for liveTrail*****
    var keyspan2 = document.getElementsByClassName("close")[2];

    // When the user clicks on <span> (x), close the modal
    keyspan2.onclick = function() {
        $("#output").html("");

        $("#userInput").val("");
        $("#liveTrialModal").hide();
        localStorage.setItem("liveTrialModalShow", "");

    };

    if(localStorage.getItem("userFirst") == null || localStorage.getItem("userFirst") == "") {
        localStorage.setItem("userFirst", "HIT");
        //to check the input for the live view
        $('#userInput').keyup(function (e) {
            if (e.which == 13) {

                var keySet = localStorage.getItem("keys").split(",");
                var outputSet = localStorage.getItem("output").split(",");
                var verbSet = localStorage.getItem("verbs").split("*");
                var nounSet = localStorage.getItem("nouns").split("*");

                var eventObj = {
                    "name": localStorage.getItem("eventDescription"),
                    "keySet": keySet,
                    "output": outputSet,
                    "verbs" : verbSet,
                    "nouns" : nounSet
                };

                checkInput(eventObj);
            }
        });
    }

}

function fillDropDown(){
    resethtml();
    if ($("#keyCheckBox").is(":checked")) {
        var options = [localStorage.getItem("adId")];

        function callback(tx, results) {
            var htmlcode = "";
            var localStore = "";
            for (var i = 0; i < results.rows.length; i++) {
                var row = results.rows[i];
                localStore += row['id'] + ", " +
                    row['description'] + "*";

                htmlcode += "<option value=" +
                    row['id'] + ">" + row['description'] + "</option>";

            }

            localStorage.setItem("eventList", localStore);

            htmlcode = $("#eventDrop").html() + htmlcode;
            var dD = $("#eventDrop");
            dD.html(htmlcode);

            $('#eventDrop').trigger('change', true);
            $("#keyEvent").show();
            $("#keyAction").hide();

        }

        textAdEvent.selectAllEvents(options, callback);
    }
    else {
    $("#keyEvent").hide();
    $("#keyAction").show();
    }
}

function fillDropDownUpdate(connectedEvent){
    var options = [localStorage.getItem("adId")];

    function callback(tx, results) {
        var htmlcode = "";
        var localStore = "";
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];
            localStore += row['id'] + ", " +
                row['description'] + "*";

            htmlcode += "<option value=" +
                row['id'] + ">" + row['description'] + "</option>";

        }

        localStorage.setItem("eventList", localStore);

        htmlcode = $("#eventDrop").html() + htmlcode;
        var dD = $("#eventDrop");
        dD.html(htmlcode);


        $("#eventDrop  option[value=" + connectedEvent + "]").prop('selected', true);
        $('#eventDrop').trigger('change', true);

        $("#keyModal").show();
        //$('#eventDrop').trigger('change', true);
        //$("#keyEvent").show();
        //$("#keyAction").hide();

    }

    textAdEvent.selectAllEvents(options, callback);
}

function liveViewClick(){
    var event = localStorage.getItem("eventId");
    var options = [localStorage.getItem("adId"), 1];

    function callback(tx, results){
        var row = results.rows[0];

        $("#liveAdventureName").html("Adventure: " + row['name']);
        getEventInfo(event);

    }
    textAdventure.select(options, callback);
}

function resethtml(){
  $("#eventDrop").html("");
  $("#eventDrop").html("<option value=\"0\" selected>...Please select one of the events</option>");
}
  /*function formReset() {
    $("form")[0].reset();
    document.getElementById('RWmyRatings').hidden = true;
  }

  function pageShowAll() {
    RWgetReviews();
  }

  function pageFeedBack_Show() {
    $("#RWemail").val(localStorage.getItem("email"));
    document.getElementById('RWmyRatings').hidden = true;
  }

  function pageFeedBack_Type() {
    RWupdateTypesDropdown();
  }

  function pageFeedBackReview_Show() {
    //RWupdateDropDownReview(localStorage.getItem("id"));
    RWshowCurrentReview()
  }

  function SaveEmail() {

    //Set the localStorage item known as "email" on the browser to the
    //value of the element on the page with an id of "RWEmailSettings"
    localStorage.setItem("email", document.getElementById('RWEmailSettings').value);

    //Set a alert box on the browser displaying the email on the local storage
    //that was just saved
    alert(localStorage.getItem("email"));
  }

  function AddCalc() {
    //this function is only for the add page (or page 2)

    //Get the values for the store quality, service and value
    //and store them in variables
    var quality = parseInt($("#RWStoreQuality").val());
    var service = parseInt($("#RWService").val());
    var value = parseInt($("#RWValue").val());

    //This section checks to see if any of the three
    //variables are in fact the correct values they
    //need to be (0-5)
    var i = 0;
    while (i < 3) {

      //define a variable called myBool
      var myBool;

      //If i has a value of 0 then check to see if
      //the variable quality is a number by calling
      //the checkNumber function and passing in the
      //value of quality
      if (i == 0) {
        myBool = checkNumber(quality);

        //if the quality is not a number that is valid
        //set qualities value to 0
        if (myBool == false) {
          quality = 0;

        }

      }

      //If i has a value of 1 then check to see if
      //the variable service is a number by calling
      //the checkNumber function and passing in the
      //value of service
      else if (i == 1) {
        myBool = checkNumber(service);

        //if the service is not a number that is valid
        //set service value to 0
        if (myBool == false) {
          service = 0;

        }

      }

      //If i has a value of 2 then check to see if
      //the variable value is a number by calling
      //the checkNumber function and passing in the
      //value of value
      else {

        myBool = checkNumber(value);

        //if the value is not a number that is valid
        //set "value" value to 0
        if (myBool == false) {
          value = 0;

        }

      }

      //increment i by 1
      i++;
    }


    //calculate the average by firsting adding all three of the values
    //together (quality, service, and value) and then divide
    //that number by three. The outcome will be the overall average
    var reviewTotal = parseInt(quality + service + value);
    var reviewAvg = parseInt(reviewTotal / 3);


    //set the value of the overall average textbox to the value of
    //reviewAvg
    $("#rwRating").val(reviewAvg);

  }


  function ReviewCalc() {

    //this function is only used for the Modify feedback page (known as 3.1)

    //Get the values for the store quality, service and value
    //and store them in variables

    var quality = parseInt($("#RWStoreQuality_review").val());
    var service = parseInt($("#RWService_Review").val());
    var value = parseInt($("#RWValue_review").val());

    //This section checks to see if any of the three
    //variables are in fact the correct values they
    //need to be (0-5)
    var i = 0;
    while (i < 3) {

      var myBool;

      //If i has a value of 0 then check to see if
      //the variable quality is a number by calling
      //the checkNumber function and passing in the
      //value of quality
      if (i == 0) {
        myBool = checkNumber(quality);

        //if the quality is not a number that is valid
        //set qualities value to 0

        if (myBool == false) {
          quality = 0;

        }

      }

      //If i has a value of 1 then check to see if
      //the variable service is a number by calling
      //the checkNumber function and passing in the
      //value of service
      else if (i == 1) {

        myBool = checkNumber(service);

        //if the service is not a number that is valid
        //set service value to 0
        if (myBool == false) {
          service = 0;

        }

      }

      //If i has a value of 2 then check to see if
      //the variable value is a number by calling
      //the checkNumber function and passing in the
      //value of value
      else {

        myBool = checkNumber(value);

        //if the value is not a number that is valid
        //set "value" value to 0
        if (myBool == false) {
          value = 0;

        }

      }

      //increment i by 1
      i++;
    }

    //calculate the average by firsting adding all three of the values
    //together (quality, service, and value) and then divide
    //that number by three. The outcome will be the overall average

    var reviewTotal = parseInt(quality + service + value);
    var reviewAvg = parseInt(reviewTotal / 3);

    //set the value of the overall average textbox to the value of
    //reviewAvg
    $("#RWrating_review").val(reviewAvg);

  }

  function checkNumber(num) {

    //if the number passed in is between 0 and 5 return true
    //meaning it is a valid number
    if (num >= 0 && num <= 5) {
      return true;
    }

    //if the number passed in is out of the range of 0-5 then return false
    //meaning it is not a valid number
    else {
      return false;
    }
  }

  function ButtonAdd_Click() {
    RWaddFeedback();
  }

  function ButtonUpdate_Click() {
    RWupdateFeedback();
  }

  function ButtonDelete_Click() {
    RWdeleteFeedback();
  }

  function ButtonDeleteDatabase_Click() {
    RWclearDatabase();
  }*/
