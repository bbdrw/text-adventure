//Created By Ryan Wing

function doAddValidation(action){
    //this function is only for the add page (or page 2)

    //if the checkbox is checked then run the below code
    if(action === 1){

        //create a form variable and setting it to the form on the page with an
        //id of "RWAddForm"

        var form = $("#RWAddForm");
        //Validate that form
        form.validate({
            //set rules for each form attribute
            rules:{
                //RWBusinessName - The attribute is required and the
                //length of the string must be between 2 to 20 characters long
                RWbusinessName:{
                    required: true,
                    rangelength: [2,20]

                },
                //RWemail - The attribute is required and the
                //function emailCheck is called to ensure that
                //the email provided is valid by returning true.
                RWemail:{
                    required: true,
                    emailcheck: true
                },
                //ReviewDate - The attribute is required
                RWReviewDate:{
                    required: true
                },
                //RWStoreQuality - Call the function numCheck and the return must be true
                RWStoreQuality:{
                    numcheck: true
                },
                //RWService- Call the function numCheck and the return must be true
                RWService:{
                    numcheck: true
                },
                //RWValue- Call the function numCheck and the return must be true
                RWValue:{
                    numcheck: true
                }
            },
            //set predefined messages if an attribute is invalid
            messages:{
                //RWBusinessName - If the textbox is empty then set the message to
                //required (with its defined message)
                //If the textbox has input but it doesnt meet the range set the message
                //to rangeLength
                RWbusinessName:{
                    required: "You must enter a business name",
                    rangelength: "length must be 2-20 characters long"
                },
                //RWemail - If the textbox is empty then set the message to
                //required (with its defined message)
                //If the email provided is invalid then set the message to
                //emailCheck
                RWemail:{
                    required: "You must enter an email",
                    emailcheck: "Please enter a valid email address."
                },
                //ReviewDate - If the textbox is empty then set the message to
                //required (with its defined message)
                RWReviewDate:{
                    required: "Review Date is required"
                },
                //RWStoreQuality - If the user has not entered a number between
                //0 and 5 set the message to numcheck
                RWStoreQuality:{
                   numcheck: "Value must be 0-5"
                },
                //RWService - If the user has not entered a number between
                //0 and 5 set the message to numcheck
                RWService:{
                    numcheck: "Value must be 0-5"
                },
                //RWValue - If the user has not entered a number between
                //0 and 5 set the message to numcheck
                RWValue:{
                    numcheck: "Value must be 0-5"
                }
            }
        });
    }
    //if the checkbox is not checked run the below code
    else{

        //create a form variable and setting it to the form on the page with an
        //id of "RWAddForm"
        var form = $("#RWAddForm");

        //Validate that form
        form.validate({
            //set rules for each form attribute
            rules:{
                //RWBusinessName - The attribute is required and the
                //lenght of the string must be between 2 to 20 characters long
                RWbusinessName:{
                    required: true,
                    rangelength: [2,20]

                },
                //RWemail - The attribute is required and the
                //function emailCheck is called to ensure that
                //the email provided is valid by returning true.
                RWemail:{
                    required: true,
                    emailcheck: true
                },
                //ReviewDate - The attribute is required
                RWReviewDate:{
                    required: true
                }
            },
            //set predefined messages if an attribute is invalid
            messages:{
                //RWBusinessName - If the textbox is empty then set the message to
                //required (with its defined message)
                //If the textbox has input but it doesnt meet the range set the message
                //to rangeLength
                RWbusinessName:{
                    required: "You must enter a business name",
                    rangelength: "length must be 2-20 characters long"
                },
                //RWemail - If the textbox is empty then set the message to
                //required (with its defined message)
                //If the email provided is invalid then set the message to
                //emailCheck
                RWemail:{
                    required: "You must enter an email",
                    emailcheck: "Please enter a valid email address."
                },
                //ReviewDate - If the textbox is empty then set the message to
                //required (with its defined message)
                RWReviewDate:{
                    required: "Review Date is required"
                }
            }
        });
    }

    //return the outcome of the validation. If the form passes without any
    //invalid inputs return that it is valid and let the info be saved
    return form.valid();

}



function doUpdateValidation(action){
    //this function is only used for the Modify feedback page (known as 3.1)

    //create a form variable and setting it to the form on the page with an
    //id of "RWEditForm"
    var form = $("#RWEditForm");

    //if the checkbox is checked then run the below code
    if(action === 1){
        //Validate that form
        form.validate({
            //set rules for each form attribute
            rules:{
                //RWbusinessName_Review - The attribute is required and the
                //length of the string must be between 2 to 20 characters long
                RWbusinessName_Review:{
                    required: true,
                    rangelength: [2,20]

                },
                //RWemail_Review - The attribute is required and the
                //function emailCheck is called to ensure that
                //the email provided is valid by returning true.
                RWemail_Review:{
                    required: true,
                    emailcheck: true
                },
                //ReviewDateReviewer - The attribute is required
                RWReviewDateReviewer:{
                    required: true
                },
                //RWStoreQuality_review - Call the function numCheck and the return must be true
                RWStoreQuality_review:{
                    numcheck: true
                },
                //RWService_Review - Call the function numCheck and the return must be true
                RWService_Review:{
                    numcheck: true
                },
                //RWValue_review- Call the function numCheck and the return must be true
                RWValue_review:{
                    numcheck: true
                }
            },
            //set predefined messages if an attribute is invalid
            messages:{
                //RWBusinessName_Review - If the textbox is empty then set the message to
                //required (with its defined message)
                //If the textbox has input but it doesnt meet the range set the message
                //to rangeLength
                RWbusinessName_Review:{
                    required: "You must enter a business name",
                    rangelength: "length must be 2-20 characters long"
                },
                //RWemail_Review - If the textbox is empty then set the message to
                //required (with its defined message)
                //If the email provided is invalid then set the message to
                //emailCheck
                RWemail_Review:{
                    required: "You must enter an email",
                    emailcheck: "Please enter a valid email address."
                },
                //ReviewDateReviewer - If the textbox is empty then set the message to
                //required (with its defined message)
                RWReviewDateReviewer:{
                    required: "Review Date is required"
                },
                //RWStoreQuality_review - If the user has not entered a number between
                //0 and 5 set the message to numcheck
                RWStoreQuality_review:{
                    numcheck: "Value must be 0-5"
                },
                //RWService_Review - If the user has not entered a number between
                //0 and 5 set the message to numcheck
                RWService_Review:{
                    numcheck: "Value must be 0-5"
                },
                //RWValue_Review - If the user has not entered a number between
                //0 and 5 set the message to numcheck
                RWValue_review:{
                    numcheck: "Value must be 0-5"
                }
            }
        });
    }
    //if the checkbox is not checked run the below code
    else{
        //Validate that form
        form.validate({
            //set rules for each form attribute
            rules:{
                //RWbusinessName_Review - The attribute is required and the
                //length of the string must be between 2 to 20 characters long
                RWbusinessName_Review:{
                    required: true,
                    rangelength: [2,20]

                },
                //RWemail_Review - The attribute is required and the
                //function emailCheck is called to ensure that
                //the email provided is valid by returning true.
                RWemail_Review:{
                    required: true,
                    emailcheck: true
                },
                //ReviewDateReviewer - The attribute is required
                RWReviewDateReviewer:{
                    required: true
                }
            },
            messages:{
                //RWBusinessName_Review - If the textbox is empty then set the message to
                //required (with its defined message)
                //If the textbox has input but it doesnt meet the range set the message
                //to rangeLength
                RWbusinessName_Review:{
                    required: "You must enter a business name",
                    rangelength: "length must be 2-20 characters long"
                },
                //RWemail_Review - If the textbox is empty then set the message to
                //required (with its defined message)
                //If the email provided is invalid then set the message to
                //emailCheck
                RWemail_Review:{
                    required: "You must enter an email",
                    emailcheck: "Please enter a valid email address."
                },
                //ReviewDateReviewer - If the textbox is empty then set the message to
                //required (with its defined message)
                RWReviewDateReviewer:{
                    required: "Review Date is required"
                }
            }
        });
    }

    //return the outcome of the validation. If the form passes without any
    //invalid inputs return that it is valid and let the info be saved
    return form.valid();

}

//when this method gets called, by the name "emailcheck" see if the passed in
//element value is a proper email by seeing if it matches the regex defined.
//If it is not return a message that the value is not valid
jQuery.validator.addMethod("emailcheck",
    function(value, element){
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return this.optional(element) || regex.test(value);
    },
    "Must be valid email address");

//when this method gets called, by the name "emailcheck" see if the passed in
//element value is a number between 0 and 5. If it is not return false and if it is
//return true. If the number is not valid set a message that the value provided does not
//equla a value that is 0-5
jQuery.validator.addMethod("numcheck",
    function(value, element){
        var num = value;
        if(num < 0 || num > 5 || num === "" || num == null){
            return false;
        }
        return true
    },
    "Value must be 0-5");