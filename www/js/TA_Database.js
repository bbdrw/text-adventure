/**
 * File Name: database.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-02 : Created
 */


var db;

/**
 * General purpose error handler
 * @param tx The transaction object
 * @param error The error object
 */
function errorHandler(tx, error) {
    console.error("SQL Error: " + tx + " (" + error.code + ") : " + error.message);
}



var DB = {
    createDatabase: function(){
        var name= "Text Ad";
        var version = "1.0";
        var displayName = "DB for Text Ad app";
        var size =  2 * 1024 * 1024;

        function successCreate() {
            console.info("Success: Database created successfully");
        }

        db = openDatabase(name, version, displayName, size, successCreate);
    },
    createTables: function(){
        function txFunction(tx) {

            var sql = "CREATE TABLE IF NOT EXISTS User( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "username VARCHAR(20) NOT NULL, " +
                "name VARCHAR(20) NOT NULL, " +
                "email VARCHAR(200) NOT NULL);";

            var sql2 = "INSERT INTO User (username,name,email) VALUES('rwing95','Ryan','ryan_666@outlook.com');";

            var sql3 = "CREATE TABLE IF NOT EXISTS Adventure( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "userId INTEGER NOT NULL, " +
                "name VARCHAR(50) NOT NULL, " +
                "description VARCHAR(255), " +
                "FOREIGN KEY(userId) REFERENCES User(id));";

            var sql4 = "CREATE TABLE IF NOT EXISTS Event( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "adId INTEGER NOT NULL," +
                "description VARCHAR(500), " +
                "img VARCHAR(500), " +
                "FOREIGN KEY(adId) REFERENCES Adventure(id));";

            var sql5 = "CREATE TABLE IF NOT EXISTS Key( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "eventId INTEGER NOT NULL," +
                "name VARCHAR(255), " +
                "action VARCHAR(255), " +
                "connectedEvent VARCHAR(255), " +
                "connectedEventName VARCHAR(255), " +
                "verb VARCHAR(255), " +
                "noun VARCHAR(255), " +
                "FOREIGN KEY(eventId) REFERENCES Event(eventId));";

            var sql6 = "CREATE TABLE IF NOT EXISTS ConnectedEvent( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "sendEvId VARCHAR(255) NOT NULL," +
                "receiveEvId VARCHAR(255) NOT NULL);";

            var sql7 = "INSERT INTO Adventure (userId,name) VALUES(1,'Adventure 1');";

            var sql8 = "INSERT INTO Event (adId, description) VALUES (1, 'You wonder into a forrest where a huge tree is planted with what seems to be golden apples hanging from it')";

            var sql9 = "INSERT INTO Event (adId, description) VALUES (1, 'Through the developing forrest, you come across an run down cottage, where all that seems to be empty')";

            var sql10 = "INSERT INTO Key (eventId, name, action, connectedEvent, connectedEventName, verb, noun) VALUES (1, 'walk up to tree', 'Now having a closer look the apples are a bit bigger than usual apples and have a weird but distinct smell', 'EMPTY', 'EMPTY', 'walk,go,run', 'tree')";
            var sql11 = "INSERT INTO Key (eventId, name, action, connectedEvent, connectedEventName, verb, noun) VALUES (1, 'climb tree', 'You are not close enough to the tree to climb it (you might want to walk closer to it)', 'EMPTY', 'EMPTY', 'climb', 'tree')";
            var sql12 = "INSERT INTO Key (eventId, name, action, connectedEvent, connectedEventName, verb, noun) VALUES (1, 'go around tree and continue through the forrest', 'EMPTY', '2', 'Through the developing forrest, you come across a run down cottage, empty and hollow', 'around', 'tree')";

            var sql13 = "INSERT INTO Key (eventId, name, action, connectedEvent, connectedEventName, verb, noun) VALUES (2, 'go near cottage', 'as you get closer to the cottage you smell spoiled food and in the near distance hear a shreak', 'EMPTY', 'EMPTY', 'go', 'cottage')";

            var myArray = [sql, sql2, sql3, sql4, sql5, sql6, sql7, sql8, sql9, sql10,sql11, sql12, sql13];

            for(i = 0; i < myArray.length; i++){

                var options = [];

                function successCreate() {
                    console.info("Success: Table created successfully.");
                }

                tx.executeSql(myArray[i], options, successCreate, errorHandler);

            }

        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
    dropTables: function () {

        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS User;";
            var sql2 = "DROP TABLE IF EXISTS Adventure;";
            var sql3 = "DROP TABLE IF EXISTS Event;";
            var sql4 = "DROP TABLE IF EXISTS Key;";
            var sql5 = "DROP TABLE IF EXISTS ConnectedEvent;";

            var myArray = [sql, sql2, sql3, sql4, sql5];
            for(i = 0; i < myArray.length; i++) {

                var options = [];

                function successDrop() {
                    console.info("Success: table dropped successfully");
                    //alert("database cleared successfully");
                }

                tx.executeSql(myArray[i], options, successDrop, errorHandler);
            }
        }
        function successTransaction() {
            console.info("Success: drop table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};
