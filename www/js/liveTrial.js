function getEventInfo(eventId){
    localStorage.setItem("userInputDescription", "");
    var options = [localStorage.getItem("adId"),eventId];
    var eventDescription = "";

    function callback(tx, results){
        var row = results.rows[0];

        eventDescription = row['description'];
        getKeyInfo(eventId,eventDescription);

    }
    textAdEvent.selectCopy(options, callback)
}


function getKeyInfo(eventId, eventDescription){

    var options = [eventId];
    function callback(tx, results){

        // var keys = [];
        // var output = [];

        var keys = "";
        var output = "";
        var verbs = "";
        var nouns = "";

        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            //keys[i] = row['name'];
            keys += row['name'] + ",";
            verbs += row['verb'] + "*";
            nouns += row['noun'] + "*";

            if(row['action'] == "EMPTY"){
                //output[i] = row['connectedEvent'];
                output += row['connectedEvent'] + ",";
            }
            else{
                //output[i] = row['action'];
                output += row['action'] + ",";
            }


        }
        showLiveModal(eventDescription, keys, output, verbs, nouns);

    }
    textAdKey.selectAll(options, callback);

}


function showLiveModal(eventDescription, keys, output, verbs, nouns){

    if(localStorage.getItem("liveTrialModalShow") == null || localStorage.getItem("liveTrialModalShow") == ""){
        $("#liveTrialModal").show();
    }

    setLiveInformation(eventDescription, keys, output, verbs, nouns);
}

function setLiveInformation(eventDescription, keys, output, verbs,nouns){

    if(localStorage.getItem("liveTrialModalShow") == null || localStorage.getItem("liveTrialModalShow") == ""){
        $("#output").html("<b>" + eventDescription + "</b>");
        localStorage.setItem("liveTrialModalShow", "Open");
    }
    else{
        $("#output").html($("#output").html() + "<b> >" + eventDescription + "</b>");
    }

    localStorage.setItem("eventDescription", eventDescription);
    localStorage.setItem("keys", keys);
    localStorage.setItem("output", output);
    localStorage.setItem("verbs", verbs);
    localStorage.setItem("nouns", nouns);


    $("#userInput").focus();

}


function checkInput(eventObj){

    var userInput = "";

    try{
        if($("#userInput").val() == null || $("#userInput").val() == ""){
            userInput = "EMPTY";
        }else{
            userInput = $("#userInput").val().toLowerCase();
        }
    }
    catch(exception){
        alert(exception);
    }


    var output = "";
    var resultSet = [];
    var fullMatch = false;

    if(userInput.toLowerCase() == "repeat"){
        repeatEvent(eventObj.name);
    }
    else if(eventObj.keySet.length == 1 && eventObj.keySet[0] == ""){
        alert("no keys");
        $("#userInput").val("");
        $("#userInput").focus();
    }
    else{
        var outPutTotalMatch = "";
        //deal with the whole word itself
        for(var x = 0; x < eventObj.keySet.length; x++){

            //check to see if the userInput 100% matches a key
            var thisKey = eventObj.keySet[x];
            var thisKey = thisKey.toLowerCase();

            if(userInput == thisKey){
                outPutTotalMatch = eventObj.output[x];
                fullMatch = true;
                break;
            }

        }

        if(fullMatch === true){
            setOutput(outPutTotalMatch);
        }
        else{

           var userWords = userInput.toLowerCase();
           var counter = 0;

            userWords = userInput.split(" ");

            var outPutObj = {
              "outputName": [],
              "outputLocation": [],
              "counter": [],
              "keyName": []

            };
            //check to see if there is a matching verb in the userinput
            for(var x = 0; x < eventObj.keySet.length; x++) {

                //go through each verb in the list of verbs
               // for(var y = 0; y < eventObj.verbs.length;y++){
                    var verbs = eventObj.verbs[x].split(",");

                    //go through each word the user has inputted
                    for(var t = 0; t < userWords.length; t++){

                        //go through each individual verb in list of verbs
                        for(var i = 0; i < verbs.length; i++){

                            //if the users word equals the verb then add that
                            if(userWords[t] == verbs[i]) {

                                if (eventObj.keySet[x] in outPutObj.outputName) {
                                    outPutObj.counter[eventObj.output[x]] += 1;

                                }
                                else {

                                    outPutObj.outputName[counter] = eventObj.output[x];
                                    outPutObj.outputLocation[counter] = x;
                                    outPutObj.counter[counter] = 1;
                                    outPutObj.keyName[counter] = eventObj.keySet[x];

                                    counter++;
                                }
                            }

                        }
                    }
               // }
            }

            //check to see if foundItems has more than one entry

            //check to see if the users words match any of the nouns
            //check to see if there is a matching verb in the userinput

            var outPutObjLength = outPutObj.outputName.length;

            for(var x =0; x < outPutObjLength;x++){

                var nounLocation = outPutObj.outputLocation[x];
                var foundItem = false;

                var nouns = eventObj.nouns[nounLocation].split(",");

                for (var y = 0; y < userWords.length; y++) {

                    for (var t = 0; t < nouns.length; t++) {

                        if (userWords[y] == nouns[t]) {
                            outPutObj.counter[x] += 1;
                            foundItem = true;
                        }

                    }

                }
                if(foundItem == false){
                    outPutObj.counter[x] = 0;
                }

            }



            if(outPutObj.outputName.length == 1 && outPutObj.counter[0] >= 2){
                setOutput(outPutObj.outputName[0]);
            }
            else if(outPutObj.outputName.length > 1){

                var high = 0;
                var outputs = [];
                var counter = 0;
                //go through each
                for(var x = 0; x < outPutObj.outputName.length; x++){

                    if(outPutObj.counter[x] > 0 && outPutObj.counter[x] > high){
                        outputs = [];
                        high = outPutObj.counter[x];
                        outputs[counter] = outPutObj.outputName[x];
                    }
                    else if(outPutObj.counter[x] > 0 && outPutObj.counter[x] == high){
                        counter++;
                        outputs[counter] = outPutObj.outputName[x];
                    }
                }

                if(outputs.length == 1){
                    setOutput(outputs[0]);
                }
                else{
                    var randomNum = Math.floor(Math.random() * Math.floor(outputs.length));
                    setOutput(outputs[randomNum]);
                }


            }
            else{
                inputNotFound();
            }

        }


    }

}

function repeatEvent(eventDescription){

    if(localStorage.getItem("userInputDescription") != null && localStorage.getItem("userInputDescription") != ""){
        eventDescription = localStorage.getItem("userInputDescription");
    }

    $("#output").html($("#output").html() + "<br><br> user: <br><br> <b> >" + eventDescription + "</b>");

    $("#userInput").val("");
    $("#userInput").focus();

    var element = document.getElementById("liveBorder");
    element.scrollTop = element.scrollHeight;


}

function inputNotFound(){
    $("#output").html($("#output").html() + "<br><br> user: " + $("#userInput").val() + " <br><br> <b> > Im not aware of that choice</b>");

    $("#userInput").val("");
    $("#userInput").focus();

    var element = document.getElementById("liveBorder");
    element.scrollTop = element.scrollHeight;


}

function setOutput(result){

    if(isNaN(result) == false){

        $("#output").html($("#output").html() + "<br><br> user: " + $("#userInput").val() + "<br><br>");
        getEventInfo(result);


        $("#userInput").val("");
        $("#userInput").focus();
    }
    else{
        //action takes place
        $("#output").html($("#output").html() + "<br><br> user: " + $("#userInput").val() + "<br><br> <b> > " + result + "</b>");

        $("#userInput").val("");
        $("#userInput").focus();

        localStorage.setItem("userInputDescription", result);

        var element = document.getElementById("liveBorder");
        element.scrollTop = element.scrollHeight;
    }

}