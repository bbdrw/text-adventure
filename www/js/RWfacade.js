function RWaddFeedback(){
    //this function is only for the add page (or page 2)

    //if the ratings checkbox is checked run the below code
    if(document.getElementById('RWrate_id').checked){

        if (doAddValidation(1)) {
            //if validation is successful then fetch the info from input controls
            var businessName = $("#RWbusinessName").val();
            var ReviewerEmail = $("#RWemail").val();
            var ReviewDate = $("#RWReviewDate").val();
            var StoreQuality = $("#RWStoreQuality").val();
            var Service = $("#RWService").val();
            var Value = $("#RWValue").val();
            var Type = $("#RWSelect").val();
            var Comments = $("#RWComments").val();
            var hasRating = $("#RWrate_id").prop("checked");

            // 3. insert into table (by calling insert DAL function and supplying inputs)
            var options = [businessName, Type, ReviewerEmail, Comments, ReviewDate, hasRating, StoreQuality, Service, Value];
            function callback() {
                console.info("Success: Record inserted successfully");
                alert("New Review Added");

                $('#RWAddForm').trigger("reset");
                document.getElementById('RWmyRatings').hidden = true;

            }
            Review.RWinsert(options, callback);

        }
        else{
            //if the validation is not successful return a console error saying that the validation
            //failed - meaning the user has entered in invalid information
            console.error("Validation failed");
        }
    }
    //if the ratings checkbox is not checked then run the below code
    else{
        if (doAddValidation(2)) {
            // if validation is successful then fetch the info from input controls
            var businessName = $("#RWbusinessName").val();
            var ReviewerEmail = $("#RWemail").val();
            var ReviewDate = $("#RWReviewDate").val();
            var Type = $("#RWSelect").val();
            var Comments = $("#RWComments").val();
            var hasRating = $("#RWrate_id").prop("checked");

            var options = [businessName, Type, ReviewerEmail, Comments, ReviewDate, hasRating, null, null, null];
            function callback() {
                console.info("Success: Record inserted successfully");
                alert("New Review Added");

                $('#RWAddForm').trigger("reset");

                //RWupdateTypesDropdown();
                document.getElementById('RWmyRatings').hidden = true;

            }
            Review.RWinsert(options, callback);


        }
        else{
            //if the validation is not successful return a console error saying that the validation
            //failed - meaning the user has entered in invalid information
            console.error("Validation failed");
        }
    }
}

function RWgetRating(quality, service, value){
    //This section checks to see if any of the three
    //variables are in fact the correct values they
    //need to be (0-5)
    var i = 0;
    while(i < 3){

        var myBool;

        //If i has a value of 0 then check to see if
        //the variable quality is a number by calling
        //the checkNumber function and passing in the
        //value of quality
        if(i == 0){
            myBool = checkNumber(quality);

            //if the quality is not a number that is valid
            //set qualities value to 0

            if(myBool == false) {
                quality = 0;

            }

        }

        //If i has a value of 1 then check to see if
        //the variable service is a number by calling
        //the checkNumber function and passing in the
        //value of service
        else if(i == 1){

            myBool = checkNumber(service);

            //if the service is not a number that is valid
            //set service value to 0
            if(myBool == false){
                service = 0;

            }

        }

        //If i has a value of 2 then check to see if
        //the variable value is a number by calling
        //the checkNumber function and passing in the
        //value of value
        else{

            myBool = checkNumber(value);

            //if the value is not a number that is valid
            //set "value" value to 0
            if(myBool == false){
                value = 0;

            }

        }

        //increment i by 1
        i++;
    }

    //calculate the average by firsting adding all three of the values
    //together (quality, service, and value) and then divide
    //that number by three. The outcome will be the overall average

    var reviewTotal = parseInt(quality + service + value);
    var reviewAvg = parseInt(reviewTotal / 3);

    //set the value of the overall average textbox to the value of
    //reviewAvg
   return reviewAvg;
}

function RWgetReviews(){
    var options = [];
    function callback(tx, results) {

        var htmlcode = "";


        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            /*console.info("Id: " + row['id'] +
                " Name: " + row['name'] +
                " Full Name: " + row['fullName'] +
                " DOB: " + row['dob'] +
                " Is Friend: " + row['isFriend']);*/
            if(row["hasRating"] == "true"){
                var overallRating = RWgetRating(row['rating1'], row['rating2'], row['rating3']);

                htmlcode += "<li><a data-role='button' data-row-id=" + row['id'] + " href='#'>" +
                    "<h1>Business Name: " + row['businessName'] + "</h1>" +
                    "<p>Reviewer Email: " + row['reviewerEmail'] + "</p>" +
                    "<p>Comments: " + row['reviewerComments'] + "</p>" +
                    "<p>OverAll Rating: " + overallRating + "</p>" +
                    "</a></li>";
            }
            else{
                htmlcode += "<li><a data-role='button' data-row-id=" + row['id'] + " href='#'>" +
                    "<h1>Business Name: " + row['businessName'] + "</h1>" +
                    "<p>Reviewer Email: " + row['reviewerEmail'] + "</p>" +
                    "<p>Comments: " + row['reviewerComments'] + "</p>" +
                    "<p>OverAll Rating: 0 </p>" +
                    "</a></li>";
            }

        }

        var lv = $("#rwReviewPageList");
        lv = lv.html(htmlcode);
        lv.listview("refresh"); //important

        $("#rwReviewPageList a").on("click", clickHandler);

        function clickHandler() {
            localStorage.setItem("id", $(this).attr("data-row-id") );
            // both will work
            $.mobile.changePage("#RWEditFeedbackPage", {transition: 'none'});
            // $(location).prop('href', '#pageDetail');
        }


    }
    Review.RWselectAll(options, callback);
}

function RWupdateTypesDropdown(){
    var options = [];
    function callback(tx, results) {

        var htmlcode = "";


        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            if(row['name'] == "Other"){
                htmlcode += " <option  value="+ row['id'] + " selected> "+ row['name'] + "</option>"
            }
            else{
                htmlcode += " <option  value="+ row['id'] +">"+ row['name'] + "</option>"
            }

        }

        var lv = $("#RWSelect");
        lv = lv.html(htmlcode);
        lv.select("refresh"); //important

        //$("#RWSelectReview a").on("click", clickHandler);

        //function clickHandler() {
         //  localStorage.setItem("id", $(this).attr("data-row-id") );
            // both will work
          //  $.mobile.changePage("#RWEditFeedbackPage", {transition: 'none'});
            // $(location).prop('href', '#pageDetail');
        //}


    }
    Type.RWselectAll(options, callback);
}

function RWupdateDropDownReview(myId){
    var options = [];
    function callback(tx, results) {

        var htmlcode = "";


        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            if(row['id'] == myId){
                htmlcode += " <option  value="+ row['id'] + " selected> "+ row['name'] + "</option>"
            }
            else{
                htmlcode += " <option  value="+ row['id'] +">"+ row['name'] + "</option>"
            }

        }

        var lv = $("#RWSelectReview");
        lv = lv.html(htmlcode);
        lv.select("refresh"); //important


    }
    Type.RWselectAll(options, callback);
}

function RWshowCurrentReview(){
    var id = localStorage.getItem("id");

    var options = [id];
    function callback(tx, results) {
        var row = results.rows[0];

        $("#RWbusinessName_Review").val(row['businessName']);

        RWupdateDropDownReview(row['typeId']);

        //$("#RWSelectReview").val(row['typeId']);

        $("#RWemail_Review").val(row['reviewerEmail']);

        $("#RWcomments_review").val(row['reviewerComments']);
        $("#RWReviewDateReviewer").val(row['reviewDate']);

        if(row['hasRating'] == "true"){
            $("#RWrate_id_Review").prop("checked", true);
            document.getElementById('RWmyRatingsReview').hidden = false;

            $("#RWStoreQuality_review").val(row['rating1']);

            $("#RWService_Review").val(row['rating2']);

            $("#RWValue_review").val(row['rating3']);

        }
        else{
            $("#RWrate_id_Review").prop("checked", false);
            document.getElementById('RWmyRatingsReview').hidden = true;
        }

        $("#txtNameModify").val(row['name']);
        $("#txtFullNameModify").val(row['fullName']);
        $("#txtDOBModify").val(row['dob']);

        if (row['isFriend'] == 'true') {
            $("#radFriendModify").prop("checked", true);
        }
        else{
            $("#radEnemyModify").prop("checked", true);
        }

        $("#frmModifyFriendEnemy :radio").checkboxradio("refresh");


    }
    Review.RWselect(options, callback);
}

function RWupdateFeedback(){
    //this function is only used for the Modify feedback page (known as 3.1)

    //if the ratings checkbox is checked run the below code
    if(document.getElementById('RWrate_id_Review').checked){
        if (doUpdateValidation(1)) {
            //if validation is successful then fetch the info from input controls
            var businessName = $("#RWbusinessName_Review").val();
            var ReviewerEmail = $("#RWemail_Review").val();
            var ReviewDate = $("#RWReviewDateReviewer").val();
            var StoreQuality = $("#RWStoreQuality_review").val();
            var Service = $("#RWService_Review").val();
            var Value = $("#RWValue_review").val();

            var Type = $("#RWSelectReview").val();
            var Comments = $("#RWcomments_review").val();
            var hasRating = $("#RWrate_id_Review").prop("checked");


            var id = localStorage.getItem("id");

            var options = [businessName, Type, ReviewerEmail, Comments, ReviewDate, hasRating, StoreQuality, Service, Value, id];
            function callback() {
                console.info("Success: Record updated successfully");
                alert("Review Updated");
                $.mobile.changePage("#RWViewFeedbackPage", {transition: 'none'});
            }
            Review.RWupdate(options, callback);

        }
        else{
            //if the validation is not successful return a console error saying that the validation
            //failed - meaning the user has entered in invalid information
            console.error("Validation failed");
        }
    }
    else{
        //if the ratings checkbox is not checked then run the below code
        if (doUpdateValidation(2)) {
            // if validation is successful then fetch the info from input controls
            var businessName = $("#RWbusinessName_Review").val();
            var ReviewerEmail = $("#RWemail_Review").val();

            var ReviewDate = $("#RWReviewDateReviewer").val();

            var Type = $("#RWSelectReview").val();
            var Comments = $("#RWcomments_review").val();
            var hasRating = $("#RWrate_id_Review").prop("checked");

            var id = localStorage.getItem("id");

            var options = [businessName, Type, ReviewerEmail, Comments, ReviewDate, hasRating, null, null, null, id];
            function callback() {
                console.info("Success: Record updated successfully");
                alert("Review Updated");
                $.mobile.changePage("#RWViewFeedbackPage", {transition: 'none'});
            }
            Review.RWupdate(options, callback);


        }
        else{

            //if the validation is not successful return a console error saying that the validation
            //failed - meaning the user has entered in invalid information
            console.error("Validation failed");
        }
    }
}

function RWdeleteFeedback(){
    var id = localStorage.getItem("id");

    var options = [id];
    function callback() {
        console.info("Success: record deleted successfully");
        alert("Review Deleted");
        $.mobile.changePage("#RWViewFeedbackPage", {transition: 'none'});

    }
    Review.RWdelete(options, callback);
}

function RWclearDatabase(){
    var result = confirm("really want to clear database?");
    if (result) {
        try {
            DB.RWdropTables();
            alert("Database cleared!");
        } catch (e) {
            alert(e);
        }


    }
}
