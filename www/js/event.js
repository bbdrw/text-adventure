
function deleteEvent(eventId, adId){
    var options = [eventId];
    function callback() {
        var options2 = [eventId, adId];
        function callback2() {
            loadEventTable(adId)
        }
        textAdEvent.delete(options2, callback2);
    }
    textAdKey.delete(options, callback);
}

function copyEvent(eventId){
    var options = [localStorage.getItem("adId"),eventId];

    var description = "";
    var keyEmpty = false;

    function callback(tx, results) {
        var row = results.rows[0];
        description = row['description'] + "COPY";

        var options2 = [eventId];
        function callback2(tx, results2) {
            var row2 = results2.rows;

            if(row2.length == 0 || row2 == null || row2 == ""){
                keyEmpty = true;
            }

            else{
                var keyArray2 = new Array(row2.length);

                for (var i = 0; i < keyArray2.length; i++){
                    keyArray2[i] = new Array(row2.length);
                }

                for (var i = 0; i < results2.rows.length ; i++) {
                    try {
                        row2 = results2.rows[i];

                        keyArray2[i][0] = row2['name'];
                        keyArray2[i][1] = row2['action'];
                        keyArray2[i][2] = row2['connectedEvent'];
                        keyArray2[i][3] = row2['connectedEventName'];
                        keyArray2[i][4] = row2['verb'];
                        keyArray2[i][5] = row2['noun'];


                    }catch(exception){
                        alert(exception.toString());
                        alert(exception.toString());
                    }
                }
            }

            //saveEvent(description, keyArray, keyEmpty);
            changeDescription(description, keyArray2, keyEmpty);

        }
        textAdKey.selectAll(options2, callback2)
    }
    textAdEvent.selectCopy(options, callback);
}

function changeDescription(description, keyArray, keyEmpty){
    var options = [description.replace('COPY', ''), localStorage.getItem("adId")];

    function callback(tx, results) {
        var eventCopyArray = [];
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];
            eventCopyArray[i] = row['description'];
            //alert(row['description']);
        }

        description = eventCopyArray[eventCopyArray.length - 1] + "COPY";
        saveEvent(description, keyArray, keyEmpty);

    }
    textAdEvent.selectEventCopy(options, callback);

}

function saveEvent(description, keyArray, keyEmpty){
    var options = [localStorage.getItem("adId"), description];
    function callback(tx, results) {
        var options = [localStorage.getItem("adId"), description];
        function callback(tx, results2) {
            var row = results2.rows[0];
            var copiedId = row['id'];
            if(keyEmpty == false){
                saveKey(copiedId, keyArray);
            }
            else{
                loadEventTable(localStorage.getItem("adId"));
            }
        }
        textAdEvent.selectEvent(options, callback);
    }
    textAdEvent.insert(options, callback);
}

function loadEventTable(adId){

    if(localStorage.getItem("eventButtonStart") == null || localStorage.getItem("eventButtonStart") == "") {
        localStorage.setItem("eventButtonStart", "STARTED");
        //******this portion is for adding events******
        var eventModal = $("#eventModal");

        $("#eventButton").on("click", function () {

            var eventName = $("#eventname").val();
            if (eventName === null || eventName === "") {
                alert("you must have an event name");
                focusElement($("#eventname"));
            }
            else {

                validateEventName(localStorage.getItem("adId"), $("#eventname").val(), eventModal);
            }

        });
    }

    if(localStorage.getItem("adventureButtonStart") == null || localStorage.getItem("adventureButtonStart") == "") {
        localStorage.setItem("adventureButtonStart", "STARTED");
        //*****Going Back to Adventure Page******//
        $("#backAdventureButton").on("click", function () {
            $.mobile.changePage("#AdventurePage", {transition: 'fade'});
            $("#adventurename").val("");
            //localStorage.setItem("firstStart", "");
            loadAdventureTable(1);

        });
    }

    $("#eventDescription").val(localStorage.getItem("eventName"));
        var options = [adId];
        localStorage.setItem("eventName", $("#eventDescription").val());

        function callback(tx, results) {
            console.info("Success: Record inserted successfully");
            var row2 = results.rows;

            var htmlcode = "<tr>" +
                "<th>Event Description</th>" +
                "<th></th>" +
                "</tr>";

            if(row2.length === 0){

                var eventTable = $("#eventTbl");
                eventTable.html(htmlcode);

                if(localStorage.getItem("firstStart") == null || localStorage.getItem("firstStart") == ""){
                    localStorage.setItem("firstStart", "finished");
                    init();
                }

            }
            else{
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows[i];

                    htmlcode += "<tr>" +
                        "<td width='45%'>" + row['description'] + "</td>" +
                        "<td width='15%' style='text-align: center;'><button type='button' data-inline='true' class='events' data-row-id=" + row['id'] + " data-role='button'>Edit</button>" +
                        "<button type='button' data-inline='true' class='delete' data-row-id=" + row['id'] + " data-role='button'>Delete</button>" +
                        "<button type='button' data-inline='true' class='copy' data-row-id=" + row['id'] + " data-role='button'>Copy</button>" +
                        "<button type='button' data-inline='true' class='liveView' data-row-id=" + row['id'] + " data-role='button'>Live View</button></td>" +
                        "</tr>";

                    var eventTable = $("#eventTbl");
                    eventTable.html(htmlcode);

                    var element = document.getElementById("eventTableDiv");
                    element.scrollTop = element.scrollHeight;

                    //****This portion is for liveTrail*****
                    var keyspan2 = document.getElementsByClassName("close")[2];

                    // When the user clicks on <span> (x), close the modal
                    keyspan2.onclick = function() {
                        $("#output").html("");

                        $("#userInput").val("");
                        $("#liveTrialModal").hide();
                        localStorage.setItem("liveTrialModalShow", "");

                    };

                    if(localStorage.getItem("userFirst") == null || localStorage.getItem("userFirst") == "") {
                        localStorage.setItem("userFirst", "HIT");

                        $('#userInput').keyup(function (e) {
                            if (e.which == 13) {

                                var keySet = localStorage.getItem("keys").split(",");
                                var outputSet = localStorage.getItem("output").split(",");
                                var verbSet = localStorage.getItem("verbs").split("*");
                                var nounSet = localStorage.getItem("nouns").split("*");

                                var eventObj = {
                                    "name": localStorage.getItem("eventDescription"),
                                    "keySet": keySet,
                                    "output": outputSet,
                                    "verbs" : verbSet,
                                    "nouns" : nounSet
                                };

                                checkInput(eventObj);
                            }
                        });
                    }

                    $(".liveView").on("click", clickHandler4);

                    function clickHandler4() {
                        var event = $(this).attr("data-row-id");
                        var options = [localStorage.getItem("adId"), 1];

                        function callback(tx, results){
                            var row = results.rows[0];

                            $("#liveAdventureName").html("Adventure: " + row['name']);
                            getEventInfo(event);

                        }
                        textAdventure.select(options, callback);


                    }

                    $(".copy").on("click", clickHandler3);

                    function clickHandler3() {
                        var event = $(this).attr("data-row-id");
                        copyEvent(event);
                    }


                    $(".delete").on("click", clickHandler2);

                    function clickHandler2() {
                        var event = $(this).attr("data-row-id");
                        deleteEvent(event, adId);
                    }

                    $(".events").on("click", clickHandler);

                    function clickHandler() {


                        localStorage.setItem("eventid", $(this).attr("data-row-id"));
                        localStorage.setItem("eventId", $(this).attr("data-row-id"));

                        var event = $(this).attr("data-row-id");

                        var options2 = [event];
                        function callback(tx, results1) {

                            var row = results1.rows[0];

                            localStorage.setItem("eventName", row['description']);
                            $("form")[0].reset();
                            $("#keyTbl").empty();

                            $("#eventModal").show();
                            $("#eventHeader").html("Edit Event");
                            $("#keyTbl").html("");
                            $("#eventDescription").val(row['description']);

                            displayKeyTable(event, 0);


                            if(localStorage.getItem("firstStart") == null || localStorage.getItem("firstStart") == ""){
                                localStorage.setItem("firstStart", "finished");
                                init();
                            }

                        }
                        textAdEvent.select(options2, callback);
                        //displayKeyModal(localStorage.getItem("keyid"));

                    }
                }
            }

        }
        textAdEvent.selectAllEvents(options, callback);
    //}

}